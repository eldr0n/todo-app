# Deployment
[https://diddlydo.herokuapp.com/](https://diddlydo.herokuapp.com/)

# install
## node modules
```bash
cd api
npm install
```
## prisma client
```bash
cd api
npx prisma generate
```
