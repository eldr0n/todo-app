async function feed(parent, args, context, info) {
  const where = args.filter
    ? {
        OR: [{ name: { contains: args.filter } }],
      }
    : {};

  const tasks = await context.prisma.task.findMany({
    where,
    skip: args.skip,
    take: args.take,
    orderBy: args.orderBy,
  });

  const count = await context.prisma.task.count({ where });

  return { tasks, count };
}

module.exports = {
  feed,
};
