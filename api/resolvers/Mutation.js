async function task(parent, args, context, info) {
  const newTask = await context.prisma.task.create({
    data: {
      name: args.name,
    },
  });

  return newTask;
}

async function complete(parent, args, context, info) {
  const compTask = await context.prisma.task.update({
    where: { id: args.id },
    data: { status: true },
  });
  return compTask;
}

async function edit(parent, args, context, info) {
  const editTask = await context.prisma.task.update({
    where: { id: args.id },
    data: { name: args.name },
  });
  return editTask;
}

async function remove(parent, args, context, info) {
  const rmTask = await context.prisma.task.delete({
    where: { id: args.id },
  });
  return rmTask;
}

module.exports = {
  task,
  complete,
  edit,
  remove,
};
