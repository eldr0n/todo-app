const { GraphQLServer } = require("graphql-yoga");
const { PrismaClient } = require("@prisma/client");
const Query = require("./resolvers/Query");
const Mutation = require("./resolvers/Mutation");
const express = require('express');
const reload = require('reload')


const prisma = new PrismaClient();

const resolvers = {
  Query,
  Mutation,
};

const server = new GraphQLServer({
  typeDefs: "schema.graphql",
  resolvers,
  context: (request) => {
    return {
      ...request,
      prisma,
    };
  },
});

server.express.use(express.static("../ui"))

const options = {
  port: 4001,
  endpoint: '/graphql',
  // subscriptions: '/subscriptions',
  playground: '/playground',
}

server.start(options, ({ port }) =>
  console.log(
    `🚀 Server started, listening on http://localhost:${port} for incoming requests.`,
  ),
)
reload(server.express)
