const app = Vue.createApp({
  data() {
    return {
      endpoint: "http://localhost:4001/graphql",
      tasks: [],
      newTask: "",
      task: {},
      search: "",
      order: "asc",
    };
  },

  watch: {
    search() {
      let searchQuery = `{ feed(filter: \"${this.search}\") { tasks { id name status createdAt } }}`;
      this.gqlFetch(searchQuery).then((data) => {
        this.tasks = data.data.feed.tasks;
      });
    },

    order() {
      this.refresh();
    },
  },

  methods: {
    getClass(status) {
      return {
        "list-group-item": true,
        "list-group-item-action": true,
        "list-group-item-success": status,
        "list-group-item-warning": !status,
      };
    },

    async gqlFetch(query) {
      let res = await fetch(this.endpoint, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ query: query }),
      });
      let data = await res.json();
      return data;
    },

    refresh() {
      const query = `{ feed(orderBy: { status: ${this.order}}) { tasks { id name status }}}`;
      this.gqlFetch(query).then((data) => {
        this.tasks = data.data.feed.tasks;
      });
    },

    addTask() {
      const mutation = `mutation { task(name: \"${this.newTask}\") { id name status }}`;
      this.gqlFetch(mutation).then(() => {
        this.refresh();
        this.newTask = "";
      });
    },

    completeTask() {
      const mutation = `mutation { complete(id: ${this.task.id}) { id name status }}`;
      this.gqlFetch(mutation).then(() => {
        this.refresh();
        this.task = {};
      });
    },

    removeTask() {
      const mutation = `mutation { remove(id: ${this.task.id}) { id name status }}`;
      this.gqlFetch(mutation).then(() => {
        this.refresh();
        this.task = {};
      });
    },

    editTask() {
      const mutation = `mutation { edit(id: ${this.task.id}, name: "${this.task.name}") { id name status }}`;
      this.gqlFetch(mutation).then(() => {
        this.refresh();
        this.task = {};
      });
    },
  },

  created() {
    this.refresh();
  },
});

app.config.devtools = true;
app.mount("#app");
