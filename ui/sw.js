importScripts("./js/idb-keyval.js");
let cacheName = "diddly-do";
// Cache our known resources during install
self.addEventListener("install", (event) => {
  event.waitUntil(
    caches
      .open(cacheName)
      .then((cache) =>
        cache.addAll([
          "./css/main.css",
          "./index.html",
          "./js/idb-keyval.js",
          "./js/App.js",
        ])
      )
  );
});

// Call Activate Event
self.addEventListener("activate", (event) => {
  console.log("Service Worker: Activated");
  // Remove unwated caches
  event.waitUntil(
    caches.keys().then((cacheName) => {
      return Promise.all(
        cacheName.map((chache) => {
          if (chache !== cacheName) {
            console.log("Service Worker: Clearing Old Cache");
            return caches.delete(chache);
          }
        })
      );
    })
  );
});

// Cache any new resources as they are fetched
self.addEventListener("fetch", (event) => {
  event.respondWith(
    caches
      .match(event.request, { ignoreSearch: true })
      .then(function (response) {
        if (response) {
          return response;
        }
        let requestToCache = event.request.clone();
        return fetch(requestToCache).then(function (response) {
          if (!response || response.status !== 200) {
            return response;
          }
          let responseToCache = response.clone();
          caches.open(cacheName).then((cache) => {
            cache.put(requestToCache, responseToCache);
          });
          return response;
        });
      })
  );
});

self.addEventListener("fetch", (event) => {
  if (
    event.request.method === "GET" &&
    event.request.headers.get("accept").includes("text/html")
  ) {
    event.respondWith(
      fetch(event.request.url).catch((error) => {
        return caches.match(offlineUrl);
      })
    );
  } else {
    event.respondWith(fetch(event.request));
  }
});

self.addEventListener("fetch", (event) => {
  event.respondWith(
    caches.open(cacheName).then((cache) => {
      return cache.match(event.request).then((response) => {
        return (
          response ||
          fetch(event.request).then((response) => {
            cache.put(event.request, response.clone());
            return response;
          })
        );
      });
    })
  );
});

self.addEventListener("sync", (event) => {
  if (event.tag === "new-Task") {
    event.waitUntil(
      idbKeyval.get("sendTask").then((value) =>
        fetch("/graphql", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ query: value }),
        })
      )
    );
    idbKeyval.delete("sendTask");
  }
});
